#include <stdio.h>
#include <stdlib.h>

int main()
{

    int a = 3;
    printf("Valor de a: %d", a); // Irá escrever "Valor de a: 3" - %d será substituído pelo valor de a, interpretado como um inteiro
    int b = 1;
    printf("Valor de a e b: %d e %d", a, b); // Irá escrever "Valor de a e b: 3 e 1
    return 0;
}
